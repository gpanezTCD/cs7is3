#!/bin/sh
echo 'Setting up IRWS environment'
set -x
trap read debug

sudo add-apt-repository universe
sudo apt-get update
sudo apt-get install openjdk-11-jdk

cd ~/workspace/cs7is3/common

cd ~/workspace
mkdir common

# decompressing lucene
cd ~/workspace/common
cp ~/workspace/cs7is3/common/lucene-7.5.0.tar lucene-7.5.0.tar
tar -xvzf lucene-7.5.0.tar

# decompressing maven
cd ~/workspace/common
cp ~/workspace/cs7is3/common/apache-maven-3.5.4-bin.tar apache-maven-3.5.4-bin.tar
tar -xvzf apache-maven-3.5.4-bin.tar

# setting path for java and maven
sudo cp java-path.sh /etc/profile.d/java-path.sh
sudo cp maven-path.sh /etc/profile.d/maven-path.sh

cd ~
echo 'export CLASSPATH=~/workspace/common/lucene-7.5.0/core/lucene-core-7.5.0.jar' >> ~/.bashrc
echo 'export CLASSPATH=~/workspace/common/lucene-7.5.0/queryparser/lucene-queryparser-7.5.0.jar' >> ~/.bashrc
echo 'export CLASSPATH=~/workspace/common/lucene-7.5.0/analysis/common/lucene-analyzers-common-7.5.0.jar' >> ~/.bashrc
echo 'export CLASSPATH=~/workspace/common/lucene-7.5.0/demo/lucene-demo-7.5.0.jar' >> ~/.bashrc

echo "Done"
set +x
exit 0