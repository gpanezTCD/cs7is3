package ie.tcd.panezveg.SearchEngine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

class ParseDoc {
  public static final int ID_MARK_OFFSET = 3;
  public static final int FIELD_MARK_LENGTH = 2;
  private String id;
  private String title;
  private String author;
  private String biblio;
  private String words;
  
  public enum DocField {
    ID(".I", "Id"), TITLE(".T", "Title"), AUTHOR(".A", "Author"), BIBLIO(".B", "Bibliography"), WORDS(".W", "Words");

    private String mark = "";
    private String label = "";

    public String getMark() {
      return this.mark;
    }
    
    public String getLabel() {
      return this.label;
    }

    private DocField(String mark, String label) {
      this.mark = mark;
      this.label = label;
    }
    
    public static DocField getDocField(String strField) {
      DocField docField;
      if (strField.equals(DocField.TITLE.getMark())) {
        docField = DocField.TITLE;
      }
      else if (strField.equals(DocField.AUTHOR.getMark())){
        docField = DocField.AUTHOR;
      }
      else if (strField.equals(DocField.BIBLIO.getMark())){
        docField = DocField.BIBLIO;
      }
      else if (strField.equals(DocField.WORDS.getMark())){
        docField = DocField.WORDS;
      }
      else {
        throw new RuntimeException();
      }
      return docField;
    }
  }

  public ParseDoc(String id) {
    this.id = id;
    this.title = "";
    this.author = "";
    this.biblio = "";
    this.words = "";
  }
  
  public String getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public String getAuthor() {
    return author;
  }

  public String getBiblio() {
    return biblio;
  }

  public String getWords() {
    return words;
  }
  
  public void addToField(DocField docField, String str) {
    switch (docField) {
      case TITLE:
        this.title = this.title.concat(str);
        break;
      case AUTHOR:
        this.author = this.author.concat(str);
        break;
      case BIBLIO:
        this.biblio = this.biblio.concat(str);
        break;
      case WORDS:
        this.words = this.words.concat(str);
        break;
      default:
        throw new RuntimeException();
    }
  }
}

class ParseQuery {
  public static final int ID_MARK_OFFSET = 3;
  public static final int FIELD_MARK_LENGTH = 2;

  private int querySeq;
  private String queryId;
  private StringBuilder words;
  private List<QueryResult> results;

  public enum QueryField {
    ID(".I"), WORDS(".W");

    private String mark = "";

    public String getMark() {
      return this.mark;
    }

    private QueryField(String mark) {
      this.mark = mark;
    }
  }
  
  public void setQuerySeq(int querySeq) {
    this.querySeq = querySeq;
  }

  public int getQuerySeq() {
    return querySeq;
  }

  public ParseQuery(String queryId) {
    this.queryId = queryId;
    this.words = new StringBuilder();
  }

  public String getQueryId() {
    return queryId;
  }

  public void addWords(StringBuilder words) {
    this.words.append(" ");
    this.words.append(words);
  }

  public String getWords() {
    return this.words.toString();
  }

  public void setResults(List<QueryResult> results) {
    this.results = results;
  }

  public List<QueryResult> getResults() {
    return results;
  }

  public static boolean isValidField(String fieldMark) {
    boolean validField = fieldMark.equals(ParseQuery.QueryField.ID.getMark()) ||
                         fieldMark.equals(ParseQuery.QueryField.WORDS.getMark());
    return validField;
  }
}

class QueryResult {
  private String docId;
  private String score;

  public QueryResult(String docId, String score) {
    this.docId = docId;
    this.score = score;
  }

  public String getDocId() {
    return docId;
  }

  public String getScore() {
    return score;
  }
}

class AppUtils {

  public static List<ParseDoc> parseCorpus(String path) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(path));
    String lastLineRead;
    
    lastLineRead = reader.readLine();
    List<ParseDoc> docs = new ArrayList<ParseDoc>();
    while (true) {
      lastLineRead = parseDoc(lastLineRead, reader, docs);
      if (lastLineRead == null) {
        break;
      }
    }
    // Close file
    reader.close();
    return docs;
  }

  // shouldn't be called with lastLineRead null
  private static String parseDoc(String lastLineRead, BufferedReader reader, List<ParseDoc> docs) throws IOException {
    ParseDoc parseDoc;
    // process id header
    if (lastLineRead.startsWith(ParseDoc.DocField.ID.getMark())) {
      parseDoc = new ParseDoc(lastLineRead.substring(ParseDoc.ID_MARK_OFFSET));
    }
    else {
      throw new RuntimeException();
    }

    // reading doc content past id field
    lastLineRead = reader.readLine();;
    
    if (lastLineRead == null) {
      // there should be content post id doc
      throw new RuntimeException();
    }
    
    // while there are valid fields within this doc
    while (true) {
      lastLineRead = parseDocField(lastLineRead, reader, parseDoc);
      
      if (lastLineRead == null || lastLineRead.startsWith(ParseDoc.DocField.ID.getMark())) {
        break;
      }
    }
    docs.add(parseDoc);
    return lastLineRead;
  }

  // throws exception if field doesn't follow format
  private static String parseDocField(String lastLineRead, BufferedReader reader, ParseDoc parseDoc) throws IOException {
    //processing field header
    String strField = lastLineRead.substring(0, ParseDoc.FIELD_MARK_LENGTH);
    StringBuilder sb = new StringBuilder();
    
    // processing field content, while field doesn't end
    String currentLine;
    while (true) {
      currentLine = reader.readLine();
      if (currentLine == null) {
        break;
      }

      if (currentLine.startsWith(ParseDoc.DocField.ID.getMark()) ||
        currentLine.startsWith(ParseDoc.DocField.TITLE.getMark()) ||
        currentLine.startsWith(ParseDoc.DocField.AUTHOR.getMark()) ||
        currentLine.startsWith(ParseDoc.DocField.BIBLIO.getMark()) ||
        currentLine.startsWith(ParseDoc.DocField.WORDS.getMark())) {
        break;
      }
      sb.append(" ");
      sb.append(currentLine);
    }
    parseDoc.addToField(ParseDoc.DocField.getDocField(strField), sb.toString());
    return currentLine;
  }

  public static List<ParseQuery> parseQueries(String path) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(path));
    String lastLineRead;
    
    lastLineRead = reader.readLine();
    List<ParseQuery> queries = new ArrayList<ParseQuery>();
    while (true) {
      lastLineRead = parseQuery(lastLineRead, reader, queries);
      if (lastLineRead == null) {
        break;
      }
    }
    // Close file
    reader.close();
    return queries;
  }

  // shouldn't be called with lastLineRead null
  private static String parseQuery(String lastLineRead, BufferedReader reader, List<ParseQuery> queries) throws IOException {
    // Create a new query object
    ParseQuery qry;
    
    // process id header
    if (lastLineRead.startsWith(ParseQuery.QueryField.ID.getMark())) {
      qry = new ParseQuery(lastLineRead.substring(ParseDoc.ID_MARK_OFFSET));
    }
    else {
      throw new RuntimeException();
    }

    // reading query content past id field
    lastLineRead = reader.readLine();
    
    if (lastLineRead == null) {
      // there should be content post query id 
      throw new RuntimeException();
    }
    
    // while there are valid fields within this query doc
    while (true) {
      lastLineRead = parseQueryField(lastLineRead, reader, qry);
      
      if (lastLineRead == null || lastLineRead.startsWith(ParseQuery.QueryField.ID.getMark())) {
        queries.add(qry);
        break;
      }
    }
    return lastLineRead;
  }

  // throws exception if field doesn't follow format
  private static String parseQueryField(String lastLineRead, BufferedReader reader, ParseQuery parseQuery) throws IOException {
    //processing field header
    String strField = lastLineRead.substring(0, ParseQuery.FIELD_MARK_LENGTH);
    
    if (!ParseQuery.isValidField(strField)) {
      throw new RuntimeException();
    }

    StringBuilder sb = new StringBuilder();
    
    // processing field content, while field doesn't end
    String currentLine;
    while (true) {
      currentLine = reader.readLine();
      if (currentLine == null) {
        break;
      }

      strField = currentLine.substring(0, ParseQuery.FIELD_MARK_LENGTH);
      if (ParseQuery.isValidField(strField)) {
        break;
      }
      sb.append(" ");
      sb.append(currentLine);
    }
    parseQuery.addWords(sb);
    return currentLine;
  }

  public static void writeResults(String path, List<ParseQuery> queries) throws IOException {
    File file = new File(path);
    FileOutputStream fos = new FileOutputStream(file);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

    for (ParseQuery qry : queries) {
      List<QueryResult> results = qry.getResults();
      for (QueryResult result : results) {
        StringBuilder sb = new StringBuilder();
        sb.append(qry.getQuerySeq());
        sb.append(" ");
        sb.append("0");
        sb.append(" ");
        sb.append(result.getDocId());
        sb.append(" ");
        sb.append("1");
        sb.append(" ");
        sb.append(result.getScore());
        sb.append(" ");
        sb.append("STANDARD");
        bw.write(sb.toString());
        bw.newLine();
      }
    }
    bw.close();
  }
}